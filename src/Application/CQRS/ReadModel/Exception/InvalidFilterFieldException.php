<?php

namespace StraTDeS\SharedKernel\Application\CQRS\ReadModel\Exception;

use StraTDeS\SharedKernel\Application\Exception\ApplicationException;

class InvalidFilterFieldException extends ApplicationException
{

}