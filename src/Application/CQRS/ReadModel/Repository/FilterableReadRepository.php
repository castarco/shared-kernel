<?php
/**
 * This file belongs to SharedKernel project.
 *
 * Author: Alex Hernández <info@alexhernandez.info>
 *
 * For license information, view LICENSE file in the root of the project.
 */

namespace StraTDeS\SharedKernel\Application\CQRS\ReadModel\Repository;

use StraTDeS\SharedKernel\Application\CQRS\ReadModel\ValueObject\ProjectionModel;
use StraTDeS\SharedKernel\Application\CQRS\ReadModel\ValueObject\ReadModelCollection;

interface FilterableReadRepository extends ReadRepository
{
    public function findOneBy(array $criteria): ?ProjectionModel;

    public function findBy(
        array $criteria = [],
        ?int $offset = null,
        ?int $limit = null,
        ?array $orderBy = null
    ): ReadModelCollection;
}