<?php
/**
 * This file belongs to SharedKernel project.
 *
 * Author: Alex Hernández <info@alexhernandez.info>
 *
 * For license information, view LICENSE file in the root of the project.
 */

namespace StraTDeS\SharedKernel\Application\CQRS\ReadModel\ValueObject;

class ListableQueryResult extends QueryResult
{
    private $page;
    private $perPage;
    private $totalResults;
    private $totalPages;
    private $filters;
    private $sortBy;

    public function __construct(
        int $page,
        int $perPage,
        int $totalResults,
        int $totalPages,
        array $filters,
        array $orderBy
    )
    {
        $this->page = $page;
        $this->perPage = $perPage;
        $this->totalResults = $totalResults;
        $this->totalPages = $totalPages;
        $this->filters = $filters;
        $this->sortBy = $orderBy;
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function getPerPage(): int
    {
        return $this->perPage;
    }

    public function getTotalResults(): int
    {
        return $this->totalResults;
    }

    public function getTotalPages(): int
    {
        return $this->totalPages;
    }

    public function getFilters(): array
    {
        return $this->filters;
    }

    public function getSortBy(): array
    {
        return $this->sortBy;
    }
}