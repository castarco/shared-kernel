<?php
/**
 * This file belongs to SharedKernel project.
 *
 * Author: Alex Hernández <info@alexhernandez.info>
 *
 * For license information, view LICENSE file in the root of the project.
 */

namespace StraTDeS\SharedKernel\Domain\Identity;

abstract class Id implements IdInterface
{
    // TODO: change return type to `static` once we jump to PHP >= 8.0
    abstract public static function fromString(string $string): Id;
}
