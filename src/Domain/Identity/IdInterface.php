<?php
/**
 * This file belongs to SharedKernel project.
 *
 * Author: Andrés Correa Casablanca <castarco@coderspirit.xyz>
 *
 * For license information, view LICENSE file in the root of the project.
 */

namespace StraTDeS\SharedKernel\Domain\Identity;

interface IdInterface
{
    public function getId();

    public static function fromString(string $string): IdInterface;

    public function getHumanReadableId(): string;
}
