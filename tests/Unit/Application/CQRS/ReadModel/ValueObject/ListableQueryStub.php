<?php

namespace StraTDeS\SharedKernel\Tests\Unit\Application\CQRS\ReadModel\ValueObject;

use StraTDeS\SharedKernel\Application\CQRS\ReadModel\ValueObject\FieldsCollection;
use StraTDeS\SharedKernel\Application\CQRS\ReadModel\ValueObject\ListableQuery;
use StraTDeS\SharedKernel\Application\CQRS\ReadModel\ValueObject\QueryField;

class ListableQueryStub extends ListableQuery
{
    protected function getFilterableFields(): FieldsCollection
    {
        $filterableFieldsCollection = new FieldsCollection();
        $filterableFieldsCollection->addFromArray(['id' => QueryField::STRING_TYPE]);
        $filterableFieldsCollection->addQueryField(new QueryField('attributes.level', QueryField::INT_TYPE));
        $filterableFieldsCollection->addQueryField(new QueryField('attributes.float', QueryField::FLOAT_TYPE));
        $filterableFieldsCollection->addQueryField(new QueryField('attributes.bool', QueryField::BOOL_TYPE));
        $filterableFieldsCollection->addQueryField(new QueryField('attributes.array_string', QueryField::ARRAY_STRING_TYPE));
        $filterableFieldsCollection->addQueryField(new QueryField('attributes.array_int', QueryField::ARRAY_INT_TYPE));
        $filterableFieldsCollection->addQueryField(new QueryField('attributes.array_bool', QueryField::ARRAY_BOOL_TYPE));
        $filterableFieldsCollection->addQueryField(new QueryField('attributes.array_float', QueryField::ARRAY_FLOAT_TYPE));
        return $filterableFieldsCollection;
    }

    protected function getSortableByFields(): FieldsCollection
    {
        $sortableFieldsCollection = new FieldsCollection();
        $sortableFieldsCollection->addQueryField(new QueryField('id', QueryField::STRING_TYPE));
        $sortableFieldsCollection->addQueryField(new QueryField('attributes.name', QueryField::STRING_TYPE));
        return $sortableFieldsCollection;
    }
}