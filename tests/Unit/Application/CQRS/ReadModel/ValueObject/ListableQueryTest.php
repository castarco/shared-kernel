<?php
/**
 * This file belongs to SharedKernel project.
 *
 * Author: Alex Hernández <info@alexhernandez.info>
 *
 * For license information, view LICENSE file in the root of the project.
 */

namespace StraTDeS\SharedKernel\Tests\Unit\Application\CQRS\ReadModel\ValueObject;

use PHPUnit\Framework\TestCase;

class ListableQueryTest extends TestCase
{
    /**
     * @test
     */
    public function checkConstructorWithNotNullValues()
    {
        // Arrange
        $listableQuery = new ListableQueryStub(
            2,
            10,
            [
                'id' => 'foo'
            ],
            ['attributes.name' => 'asc']
        );

        // Act

        // Assert
        $this->assertEquals(2, $listableQuery->getPage());
        $this->assertEquals(10, $listableQuery->getPerPage());
        $this->assertEquals([
            'id' => 'foo'
        ], $listableQuery->getFilters());
        $this->assertEquals(['attributes.name' => 'asc'], $listableQuery->getSortBy());
    }

    /**
     * @test
     */
    public function checkConstructorWithNotNullValuesAndFilteringByAnIntField()
    {
        // Arrange
        $listableQuery = new ListableQueryStub(
            2,
            10,
            [
                'attributes.level' => "1"
            ],
            ['attributes.name' => 'asc']
        );

        // Act

        // Assert
        $this->assertEquals(2, $listableQuery->getPage());
        $this->assertEquals(10, $listableQuery->getPerPage());
        $this->assertEquals([
            'attributes.level' => 1
        ], $listableQuery->getFilters());
        $this->assertEquals(['attributes.name' => 'asc'], $listableQuery->getSortBy());
    }

    /**
     * @test
     */
    public function checkConstructorFilteringByAFloatField()
    {
        // Arrange
        $listableQuery = new ListableQueryStub(
            null,
            null,
            [
                'attributes.float' => "1.1"
            ]
        );

        // Act

        // Assert
        $this->assertEquals(1, $listableQuery->getPage());
        $this->assertEquals(20, $listableQuery->getPerPage());
        $this->assertEquals([
            'attributes.float' => 1.1
        ], $listableQuery->getFilters());
    }

    /**
     * @test
     */
    public function checkConstructorFilteringByAStringField()
    {
        // Arrange
        $listableQuery = new ListableQueryStub(
            null,
            null,
            [
                'id' => '11111111-1111-1111-1111-111111111111\'1111'
            ]
        );

        // Act

        // Assert
        $this->assertEquals(1, $listableQuery->getPage());
        $this->assertEquals(20, $listableQuery->getPerPage());
        $this->assertEquals([
            'id' => '11111111-1111-1111-1111-111111111111\'1111'
        ], $listableQuery->getFilters());
    }

    /**
     * @test
     */
    public function checkConstructorFilteringByABoolField()
    {
        // Arrange
        $listableQuery = new ListableQueryStub(
            null,
            null,
            [
                'attributes.bool' => 'false'
            ]
        );

        // Act

        // Assert
        $this->assertEquals(1, $listableQuery->getPage());
        $this->assertEquals(20, $listableQuery->getPerPage());
        $this->assertEquals([
            'attributes.bool' => false
        ], $listableQuery->getFilters());
    }

    /**
     * @test
     */
    public function checkConstructorFilteringByAsArrayStringField()
    {
        // Arrange
        $listableQuery = new ListableQueryStub(
            null,
            null,
            [
                'attributes.array_string' => [
                    '11111111-1111-1111-1111-111111111111\'1111',
                    '11111111-1111-1111-1111-111111111111\'1111'
                ]
            ]
        );

        // Act

        // Assert
        $this->assertEquals(1, $listableQuery->getPage());
        $this->assertEquals(20, $listableQuery->getPerPage());
        $this->assertEquals([
            'attributes.array_string' => [
                '11111111-1111-1111-1111-111111111111\'1111',
                '11111111-1111-1111-1111-111111111111\'1111'
            ]
        ], $listableQuery->getFilters());
    }


    /**
     * @test
     */
    public function checkConstructorFilteringByAsArrayIntField()
    {
        // Arrange
        $listableQuery = new ListableQueryStub(
            null,
            null,
            [
                'attributes.array_int' => [
                    '1',
                    '2',
                    '3'
                ]
            ]
        );

        // Act

        // Assert
        $this->assertEquals(1, $listableQuery->getPage());
        $this->assertEquals(20, $listableQuery->getPerPage());
        $this->assertEquals([
            'attributes.array_int' => [
                1,
                2,
                3
            ]
        ], $listableQuery->getFilters());
    }


    /**
     * @test
     */
    public function checkConstructorFilteringByAsArrayBoolField()
    {
        // Arrange
        $listableQuery = new ListableQueryStub(
            null,
            null,
            [
                'attributes.array_bool' => [
                    'true',
                    'false',
                    'true'
                ]
            ]
        );

        // Act

        // Assert
        $this->assertEquals(1, $listableQuery->getPage());
        $this->assertEquals(20, $listableQuery->getPerPage());
        $this->assertEquals([
            'attributes.array_bool' => [
                true,
                false,
                true
            ]
        ], $listableQuery->getFilters());
    }


    /**
     * @test
     */
    public function checkConstructorFilteringByAsArrayFloatField()
    {
        // Arrange
        $listableQuery = new ListableQueryStub(
            null,
            null,
            [
                'attributes.array_float' => [
                    '1.23',
                    '2.432',
                    '3.323'
                ]
            ]
        );

        // Act

        // Assert
        $this->assertEquals(1, $listableQuery->getPage());
        $this->assertEquals(20, $listableQuery->getPerPage());
        $this->assertEquals([
            'attributes.array_float' => [
                1.23,
                2.432,
                3.323
            ]
        ], $listableQuery->getFilters());
    }

    /**
     * @test
     */
    public function checkConstructorWithoutValues()
    {
        // Arrange
        $listableQuery = new ListableQueryStub();

        // Act

        // Assert
        $this->assertEquals(1, $listableQuery->getPage());
        $this->assertEquals(20, $listableQuery->getPerPage());
        $this->assertEquals([], $listableQuery->getFilters());
        $this->assertEquals([], $listableQuery->getSortBy());
    }

    /**
     * @test
     */
    public function checkConstructorWithNullValues()
    {
        // Arrange
        $listableQuery = new ListableQueryStub(
            null,
            null,
            null,
            null
        );

        // Act

        // Assert
        $this->assertEquals(1, $listableQuery->getPage());
        $this->assertEquals(20, $listableQuery->getPerPage());
        $this->assertEquals([], $listableQuery->getFilters());
        $this->assertEquals([], $listableQuery->getSortBy());
    }
}
