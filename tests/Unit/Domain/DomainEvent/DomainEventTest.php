<?php
/**
 * This file belongs to SharedKernel project.
 *
 * Author: Alex Hernández <info@alexhernandez.info>
 *
 * For license information, view LICENSE file in the root of the project.
 */

namespace StraTDeS\SharedKernel\Tests\Unit\Domain\DomainEvent;

use PHPUnit\Framework\TestCase;

class DomainEventTest extends TestCase
{
    /**
     * @test
     */
    public function checkFireReturnsProperValues()
    {
        // Arrange
        $id = IdStub::generate();
        $userId = IdStub::generate();
        $entityId = IdStub::generate();
        $data = [
            'foo' => 'bar'
        ];
        $createdAt = new \DateTime();
        $domainEvent = DomainEventStub::fire(
            $id,
            $entityId,
            $data,
            $createdAt,
            $userId
        );

        // Act

        // Assert
        $this->assertEquals($id, $domainEvent->getId());
        $this->assertEquals($userId, $domainEvent->getUserId());
        $this->assertEquals($entityId, $domainEvent->getEntityId());
        $this->assertEquals($data, $domainEvent->getData());
        $this->assertEquals($createdAt, $domainEvent->getCreatedAt());
        $this->assertEquals(DomainEventStub::DEFAULT_VERSION, $domainEvent->getVersion());
        $this->assertEquals(DomainEventStub::DEFAULT_CODE, $domainEvent->getCode());
    }
}
