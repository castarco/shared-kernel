<?php
/**
 * This file belongs to SharedKernel project.
 *
 * Author: Alex Hernández <info@alexhernandez.info>
 *
 * For license information, view LICENSE file in the root of the project.
 */

namespace StraTDeS\SharedKernel\Tests\Unit\Domain\DomainEvent;

use StraTDeS\SharedKernel\Domain\Identity\GenerableId;
use StraTDeS\SharedKernel\Domain\Identity\Id;

class IdStub extends GenerableId
{
    private $id;

    public function __construct(string $id)
    {
        $this->id = $id;
    }

    public static function generate(): Id
    {
        return new self((string)rand(0,10));
    }

    public function getId()
    {
        return $this->id;
    }

    public static function fromString(string $string): Id
    {
        return new self($string);
    }

    public function getHumanReadableId(): string
    {
        return $this->id;
    }
}